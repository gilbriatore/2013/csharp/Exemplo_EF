﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace Exemplo_EF
{
    class Program
    {
        static ExemploEntities context = new ExemploEntities();
        static void Main(string[] args)
        {
            int op;
            do
            {
                Console.WriteLine("\n\nExemplo Entity Framework");
                Console.WriteLine("1 - Incluir cliente");
                Console.WriteLine("2 - Alterar cliente");
                Console.WriteLine("3 - Excluir cliente");
                Console.WriteLine("4 - Consultar cliente");
                Console.WriteLine("5 - Sair");
                Console.Write("Opção: ");
                op = int.Parse(Console.ReadLine());
                switch (op)
                {
                    case 1:
                        IncluirClientes();
                        break;
                    case 2:
                        AlterarClientes();
                        break;
                    case 3:
                        ExcluirClientes();
                        break;
                    case 4:
                        ConsultarClientes();
                        break;
                }
            } while (op != 5);
        }

        private static void IncluirClientes()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("Nome: ");
            cliente.Nome = Console.ReadLine();
            Cliente temp = context.Clientes.FirstOrDefault(x => x.Nome.Equals(cliente.Nome));
            if (temp == null)
            {
                Console.Write("Idade: ");
                cliente.Idade = int.Parse(Console.ReadLine());
                context.Clientes.Add(cliente);
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Cliente já cadastrado.");
            }
        }

        private static void AlterarClientes()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("Nome: ");
            cliente.Nome = Console.ReadLine();
            cliente = context.Clientes.FirstOrDefault(x => x.Nome.Equals(cliente.Nome));
            if (cliente != null)
            {
                Console.WriteLine("Id: " + cliente.Id);
                Console.WriteLine("Idade: " + cliente.Idade);
                Console.Write("Alterar? ");
                string op = Console.ReadLine();
                if (op.Equals("s"))
                {
                    Console.Write("Idade: ");
                    cliente.Idade = int.Parse(Console.ReadLine());
                    context.Entry(cliente).State = EntityState.Modified;
                    context.SaveChanges();
                }                
            }
            else
            {
                Console.WriteLine("Cliente não cadastrado.");
            }
        }

        private static void ExcluirClientes()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("Nome: ");
            cliente.Nome = Console.ReadLine();
            cliente = context.Clientes.FirstOrDefault(x => x.Nome.Equals(cliente.Nome));
            if (cliente != null)
            {
                Console.WriteLine("Id: " + cliente.Id);
                Console.WriteLine("Idade: " + cliente.Idade);
                Console.Write("Excluir? ");
                string op = Console.ReadLine();
                if (op.Equals("s"))
                {
                    context.Clientes.Remove(cliente);
                    context.SaveChanges();
                }                
            }
            else
            {
                Console.WriteLine("Cliente não cadastrado.");
            }
        }

        private static void ConsultarClientes()
        {
            Console.Clear();
            Cliente cliente = new Cliente();
            Console.Write("Uma parte do nome: ");
            cliente.Nome = Console.ReadLine();
            IOrderedEnumerable<Cliente> clientes = context.Clientes.Where(x => x.Nome.Contains(cliente.Nome)).ToList().OrderBy(x => x.Nome);
            foreach (Cliente x in clientes)
            {
                Console.WriteLine("Id: " + x.Id);
                Console.WriteLine("Nome: " + x.Nome);
                Console.WriteLine("Idade: " + x.Idade);
                Console.WriteLine("------------------------");
            }
            Console.ReadKey();
        }
    }
}
