﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplo_EF
{
    class Cliente
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public int Idade { set; get; }
    }
}
